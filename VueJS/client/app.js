var endpoint='http://localhost/razojaba1u/ajax/VueJS/jsserver/public/';
//var url_endpoint='jsserver/public/index.php';


var app = new Vue({
		el: '#app',
		/***********************
		*equivale a onload (se ejecuta al cargar la page)
		************************/
		created(){
			axios.get(endpoint + 'products').then(function(response){
				app.produits = response.data;
			});

			axios.get(endpoint + 'cart').then(function(response){
				app.panier = response.data;
			});

			this.$on('panier-modifie', function(panier){
				this.miseAJourPanier(panier);
			});

			this.$on('produit-panier-ok', function(id){
				this.produitPanierOk(id);
			});
		},
		data : {
			produits : [],
			panier : []
		},
		methods : {
			miseAJourPanier(panier) {
				this.panier = panier;	
			},
			produitPanierOk(id){
				this.panier[id].ok = true;
				this.produitPanierOk++;
				if(this.produitsPanierOk == Object.keys(this.panier).length){
					this.panierEnAttente = false;
					this.panierOk = true;
				}else{
					this.panierEnAttente=true;
				}
			}
		}
});
