/****************************************
* Composant para la lista de productos
*****************************************/

Vue.component('list-produits', {
	props: ['produits'],
	template : '<div class="list-produits">\
					<h2>Nos produits</h2>\
					<produit v-for="unProduit in produits" :produit="unProduit"></produit>\
				</div>'
});

/****************************************
* Composant para un producto de la lista
*****************************************/

Vue.component('produit', {
	props: ['produit'],
	template : '<div class="produit">\
					<p><b> {{ produit.nom }} </b></p> \
					<p> {{ produit.description }} </p> \
					<p> {{ produit.prix }} </p> \
					<image-produit :un-produit="produit"></image-produit>\
					<bouton-ajout :un-produit="produit"></bouton-ajout>\
				</div>'
});

/****************************************
* Composant para resguardar la imagen
*****************************************/

Vue.component('imageProduit', {
	props: ['un-produit'],
	template : '<div class="image-produit" :alt="unProduit.nom" :style="\'background-image:url(\'+unProduit.image+\')\'"></div>'
});

/****************************************
* Composant para resguardar el botôn "agregar"
*****************************************/

Vue.component('bouton-ajout', {
	props : ['un-produit'],
	template : '<button @click="ajoutProduit">Ajouter au panier</button>',
	methods : {
		ajoutProduit() {
				axios.post(endpoint+'cart/'+this.unProduit.id).then(function(response){
					console.table(response.data);
					var panier = response.data;
					app.$emit('panier-modifie', panier);
					
				});
		}
	}
});

/****************************************
* Composant para resguardar panier
*****************************************/

Vue.component('panier', {
	props: ['panier'],
	template : '<div class="panier">\
					<h1>Votre panier</h1>\
					<produit-panier v-for="produit in panier" :produit="produit"></produit-panier>\
					<boutons-panier :panier="panier" v-show="panierVide"></boutons-panier>\
				</div>',
	computed : {
		panierVide(){
			return !(this.panier.length == 0);
		},
	}
});


/****************************************
* Composant para resguardar los productos del panier
*****************************************/
//AKI ME FALTA ALGO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Vue.component('produit-panier', {
	props: ['produit'],
	template : `<div class="produit-panier" :class="{ 'produit-ok':produit.ok }">
					<b>{{ produit.nom}}</b> <small>{{produit.qte}}</small> {{ produit.prix}}€
				</div>`
});


/****************************************
* Boutons Panier
*****************************************/

Vue.component('boutons-panier', {
	props: ['panier'],
	template : '<div class="boutons-panier">\
				<br><hr>\
				<input type="button" value="Commander" @click="commander"><br>\
				<input type="button" value="Vider panier" @click="viderPanier">\
				 </div>',
	methods : {
		viderPanier() {
				axios.delete(endpoint+'cart').then(function(response){
					app.miseAJourPanier(response.data);
				});
		},
		commander() {
			for(cle in this.panier){
				var produit=this.panier[cle];

				axios.put(endpoint+'cart/'+produit.id+'/buy').then(function(response){
					console.table(response.data.product);
					app.$emit('produit-panier-ok', response.data.product);
				});
			}	
		}
	}
});

